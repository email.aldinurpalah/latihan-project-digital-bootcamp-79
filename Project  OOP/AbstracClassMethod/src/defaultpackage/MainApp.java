package defaultpackage;
import childs.Item;
import parents.Product;

public class MainApp {
	Product product1 = new Product(); //Tidak dapat langsung diinstansiasi menjadi object karena class product adalah Abstract.
	Product product2 = new Item(); //Dapat membuat object product dari instansiasi childs class item 

}
