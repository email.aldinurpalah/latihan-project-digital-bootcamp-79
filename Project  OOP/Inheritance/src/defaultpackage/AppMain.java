package defaultpackage;
import childs.Doctor;
import childs.Programmer;
import childs.Teacher;
import parents.Person;

public class AppMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Person person1 = new Person();
		person1.setName("Rizki");
		person1.setAddress("Bandung");
		
		System.out.println(person1.getName());
		System.out.println(person1.getAddress());
		//Person person2 = new Teacher("Joko", "Tegal", "Matematika");
		//Person person3 = new Doctor("Eko", "Surabaya", "Pedistrician");
		
		//sayHello(person1);
		//sayHello(person2);
		//sayHello(person3);
	}
		static void sayHello(Person person) {
			String message;
			if(person instanceof Programmer) {
				Programmer programmer = (Programmer) person;
				message = "Hello, " + programmer.getName() + ". Seorang Programmer " + programmer.getTechnology() + ".";
			}else if(person instanceof Teacher){
				Teacher teacher = (Teacher) person;
				message = "Hello, " + teacher.getName() + ". Seorang Guru " + teacher.getSubject() + ".";
			}else if(person instanceof Doctor){
				Doctor doctor = (Doctor) person;
				message = "Hello, " + doctor.getName() + ". Seorang Dokter " + doctor.getSpecialist() + ".";
			}else {
				message = "Hello, " + person.getName() +".";
			}
			System.out.println(message);
		}
	//	person1.greeting();
	//	System.out.println(((Programmer)person1).technology);
		
		//object doctor menggunakan Constructor default
	//	Doctor doctor1 = new Doctor();
	//	doctor1.name = "Elis";
	//	doctor1.address = "Jakarta";
	//	doctor1.specialist = "Dentist";
		
		//object doctor menggunakan Constructor berparameter
	//	Doctor doctor2 = new Doctor("Joko", "Tegal", "Cardiologist");
		
	//	doctor1.greeting();
	//	System.out.println();
	//	doctor2.greeting();
		
	//	Person person1 = new Person();
	//	person1.name = "Hendra";
	//	person1.address = "Garut";
		
	//	Teacher teacher1 = new Teacher();
	//	teacher1.name = "Budi";
	//	teacher1.address = "Bandung";
	//	teacher1.subject = "Matematika";
		
	//	Doctor doctor1 = new Doctor();
	//	doctor1.name = "Elis";
	//	doctor1.address = "Jakarta";
	//	doctor1.specialist = "Dentis";
		
	//	Programmer programmer1 = new Programmer();
	//	programmer1.name = "Rizki";
	//	programmer1.address = "Surabaya";
	//	programmer1.technology = "Javascript";
		
	//	person1.greeting();
	//	System.out.println();
		
	//	teacher1.greeting();
	//	System.out.println();
		
	//	doctor1.greeting();
	//	System.out.println();
		
	//	programmer1.greeting();


}
