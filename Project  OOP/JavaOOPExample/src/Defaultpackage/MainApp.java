package Defaultpackage;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Membuat object dari class Person Menggunakan Constructor Parameter
		Person person1 = new Person("Eko", "Tegal");
		person1.sayHello("Padepokan 79");
		System.out.println(person1.sayAddress());
		
		
		//Membuat object dari class Person menggunakan Constructor Default
		Person person2 = new Person();
		person2.name = "Joko";
		person2.address = "Surabaya";
		person2.sayHello("Padepokan 79");
		System.out.println(person2.sayAddress());
		
		//Membuat object dari class Person menggunakan Constructor satu parameter
		Person person3 = new Person("Budi");
		person3.address = "Bandung";
		person3.sayHello("Padepokan 79");
		System.out.println(person3.sayAddress());
		
	}

}
