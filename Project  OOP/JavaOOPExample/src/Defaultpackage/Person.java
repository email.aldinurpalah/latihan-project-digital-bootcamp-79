package Defaultpackage;
public class Person {
	String name;
	String address;
	final String country = "Indonesia";
	
	//Constructor Default
	Person() {
		
	}
	
	//Constructor satu parameter
	Person(String paramName){
		name = paramName;
	}
	
	//Constructor Parameter
	Person(String name, String address){
//		this(paramName);
//		address = paramAddress;
		
		//Gunakan keyword this untuk solving variable shadowing.
		this.name = name; // ini menunjukan field name adalah dari class person itu sendiri.
		this.address = address; //ini menunjukan field address adalah dari class person itu sendiri.
	}
	
	//Method void 
	void sayHello(String paramName) {
		System.out.println("Hello " + paramName + ", Myname is " + name + ".");
	}
	
	//Method return value (Mengembalikan Nilai)
	String sayAddress() {
		return "I, come from " + address + ".";
	}

}
