package defaultpackage;

public class Categories {
	private int id;
	private String name;
	private boolean expensive;
	
	public Categories(int id, String name, boolean expensive) {
		super();
		this.id = id;
		this.name = name;
		this.expensive = expensive;
	}

	public Categories() {
		super();
	}
	
	//contoh Getter dan Setter tipe data boolean
	
	//getter
	public boolean isExpensive() {
		return this.expensive;
	}
	
	//setter
	public void setExpensive(boolean expensive) {
		this.expensive = expensive;
	}
	
	//Tipe data object
	//Getter
	public String getName() {
		return this.name;
	}
	
	//setter
	public void setName(String name) {
		this.name = name;
	}
	
	//Tipe data primitif (Integer)
	//getter
	public int getId() {
		return this.id;
	}
	
	//setter
	public void setId(int id) {
		this.id = id;
	}

}
